---
title: "SetTag"
description: "تنظیم برخی از برچسب‌ها بر روی موسیقی داده شده!"
repo: "setTag"
tags: ["برچسب‌موسیقی", "موسیقی", "پایتون"]
weight: 1
draft: false
---
